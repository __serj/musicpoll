<?php

namespace App\Http\Controllers\Api\v1;
use App\Http\Controllers\Controller;

use App\User;
use App\Vote;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class VoteController extends Controller
{
    /**
     * Accepts user's vote
     *
     * @param int $pollId
     * @return \Illuminate\Http\JsonResponse
     */
    public function increment(int $pollId)
    {
        $poll = \App\Poll::find($pollId);

        $response = [
            'data' => []
        ];

        if ($poll && $poll->status == 'opened') {
            $password = null;
            $user = \App\User::getUserOrGenerate(request()->json('phoneNumber'), request()->json('username'));
            $voteResult = \App\Vote::doVote($user, $pollId, request()->json('optionId'));

            $response['data'] = array_values(Vote::getResultByUser($pollId, $user));

            if (!$voteResult) {
                $response['warning'] = 'User has already voted in this poll.';
            }
        }
        else if ($poll && $poll->status != 'opened') {
            $response['error'] = "This poll is not opened and does not accept votes.";
        }
        else {
            $response['error'] = "Poll not found.";
        }

        return response()->json($response);
    }

    /**
     * Vote results for particular poll
     *
     * @param int $pollId
     * @return \Illuminate\Http\JsonResponse
     */
    public function result(int $pollId)
    {
        $response = [
            'data' => array_values(Vote::getResult($pollId))
        ];

        return response()->json($response);
    }

    /**
     * Vote results for particular poll
     *
     * @param int $pollId
     * @return \Illuminate\Http\JsonResponse
     */
    public function resultByUser(int $pollId, string $phoneNumber = null)
    {
        $user = User::getUserByPhone($phoneNumber);

        $result = [
            'data' => Vote::getResultByUser($pollId, $user)
        ];

        return response()->json($result);
    }
}
