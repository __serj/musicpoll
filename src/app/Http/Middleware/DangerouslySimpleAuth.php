<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Validation\UnauthorizedException;

class DangerouslySimpleAuth
{
   /**
    * Handle an incoming request.
    *
    * @param  \Illuminate\Http\Request $request
    * @param  \Closure $next
    * @return mixed
    */
   public function handle($request, Closure $next)
   {
        if (!$this->check()) {
            throw new UnauthorizedException();
        }

        return $next($request);
   }

    /**
     * @return bool
     */
   protected function check(): bool
   {
        $apiKey = config('auth.api_key');
        $requestApiKey = request()->header('api-key');

        return strcmp($apiKey, $requestApiKey) === 0;
   }

}

