<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'phone', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.

     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Fetches a user by phone number. If such user does not exist
     * then a new user with specified phone and username will be created.
     *
     * @param string $phoneNumber
     * @param string $userName
     * @return self
     */
    public static function getUserOrGenerate(string $phoneNumber, string $userName): self
    {
        $user = self::getUserByPhone($phoneNumber);

        if (!$user) {
            $password = Hash::make(str_random(12));
            $user = \App\User::create([
                'name' => $userName,
                'phone' => $phoneNumber,
                'password' => $password,
            ]);
        }

        return $user;
    }

    /**
     * @param string $phoneNumber
     * @return User
     */
    public static function getUserByPhone(string $phoneNumber): self
    {
        return \App\User::where('phone', $phoneNumber)->first();
    }
}
