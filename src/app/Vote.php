<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;

class Vote extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'votes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['option_id', 'user_id'];

    /**
     * @return HasMany
     */
    public function options(): hasMany
    {
        return $this->hasMany('App\Option', 'option_id', 'id');
    }

    public function setUpdatedAtAttribute($value)
    {
        // to Disable updated_at
    }

    /**
     * Returns option list for a particular poll
     * along with it's vote results
     *
     * @param int $pollId
     * @return array
     */
    public static function getResult(int $pollId): array
    {
        $cacheKeyResult = self::cacheKeyResult($pollId);
        $cacheKeyTotal = self::cacheKeyTotal($pollId);
        $resultAbsoluteById = Redis ::get($cacheKeyResult);

        if (!$resultAbsoluteById) {
            $optionsWithVotes = DB::select(DB::raw("
                SELECT options.id, options.title, count(*) as cnt
                FROM options
                INNER JOIN votes ON options.id = votes.option_id
                WHERE options.poll_id = :pollId
                GROUP BY options.id
            "), ['pollId' => $pollId]);

            $optionsWithVotesById = [];
            foreach ($optionsWithVotes as $k => $option) {
                $optionsWithVotesById[$option->id] = $option;
            }
            unset($optionsWithVotes);

            $resultAbsolute = DB::select(DB::raw("
                SELECT options.id, options.title
                FROM options
                WHERE options.poll_id = :pollId
            "), ['pollId' => $pollId]);

            $totalVotesCnt = 0;
            $resultAbsoluteById = [];
            foreach ($resultAbsolute as $k => $option) {
                $resultAbsoluteById[$option->id] = $option;
                $resultAbsoluteById[$option->id]->cnt = $optionsWithVotesById[$option->id]->cnt ?? 0;
                $totalVotesCnt += $resultAbsoluteById[$option->id]->cnt;
            }
            unset($resultAbsolute);

            Redis::set($cacheKeyResult, json_encode($resultAbsoluteById));
            Redis::set($cacheKeyTotal, json_encode($totalVotesCnt));
        }
        else {
            $totalVotesCnt = Redis::get($cacheKeyTotal);
            $resultAbsoluteById = json_decode($resultAbsoluteById);
        }

        $resultInPercent = [];
        foreach ($resultAbsoluteById as $k => $option) {
            $resultInPercent[$option->id] = $option;
            $result = $option->cnt == 0 ? 0 : round(($option->cnt / $totalVotesCnt) * 100 , 2);
            $resultInPercent[$option->id]->cnt = "{$result}%";
        }

        return $resultInPercent;
    }

    /**
     * Poll result with user matched data
     *
     * @param int $pollId
     * @param User|null $user
     * @return array
     */
    public static function getResultByUser(int $pollId, User $user = null): array
    {
         $userVote = null;
         if ($user) {
            $userVote = DB::selectOne(DB::raw("
                SELECT votes.option_id, options.title
                FROM votes
                INNER JOIN options ON votes.option_id = options.id
                INNER JOIN polls ON polls.id = options.poll_id
                WHERE polls.id = :pollId AND votes.user_id = :userId
            "), ['pollId' => $pollId, 'userId' => $user->id]);
         }

         $pollData = self::getResult($pollId);
         $response = [
             'result' => array_values($pollData),
             'userMatchedData' => []
         ];

         if ($userVote) {
            $response['userMatchedData'] = [
                'username' => $user->name,
                'choiceOptionId' => $userVote->option_id,
                'choiceOptionTitle' => $userVote->title,
                'choiceSimilarVolume' => $pollData[$userVote->option_id]->cnt
            ];
         }

         return $response;
    }

    /**
     * Processes user's vote
     *
     * @param User $user
     * @param int $pollId
     * @param int $optionId
     * @return bool
     */
    public static function doVote(User $user, int $pollId, int $optionId): bool
    {
        $vote = self::select('id')
            ->where([
                ['user_id', '=', $user->id],
                ['option_id', '=', $optionId]
            ])
            ->first();

        if (!$vote) {
            $vote = self::create([
                'option_id' => $optionId,
                'user_id' => $user->id
            ]);

            $cacheKeyResult = self::cacheKeyResult($pollId);
            $resultAbsoluteById = Redis ::get($cacheKeyResult);
            if ($resultAbsoluteById) {
                $resultAbsoluteById = json_decode($resultAbsoluteById);
                $resultAbsoluteById->{$optionId}->cnt += 1;
                Redis::set($cacheKeyResult, json_encode($resultAbsoluteById));
                Redis::incr(self::cacheKeyTotal($pollId));
            }

            return true;
        }

        return false;
    }

    /**
     * @param int $pollId
     * @return string
     */
    public static function cacheKeyTotal(int $pollId): string
    {
        return "pollAbsoluteResult_{$pollId}_total";
    }

    /**
     * @param int $pollId
     * @return string
     */
    public static function cacheKeyResult(int $pollId): string
    {
        return "pollAbsoluteResult_{$pollId}";
    }

}
