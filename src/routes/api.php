<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(
    [
        'middleware' => ['api.auth'],
        'prefix' => 'v1'
    ],
    function () {
        Route::post('vote/{pollId}/', 'Api\v1\VoteController@increment');
        Route::get('vote/{pollId}/', 'Api\v1\VoteController@result');
        Route::get('vote/{pollId}/{phoneNumber}', 'Api\v1\VoteController@resultByUser');
    }
);
