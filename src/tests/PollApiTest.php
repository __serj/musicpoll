<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PollApiTestTest extends TestCase
{
    use DatabaseTransactions;

    protected function createPoll()
    {
        $poll = factory(App\Poll::class)->create();

        $optionIds = [];
        $optionIds[] = factory(App\Option::class)->create(['poll_id' => $poll->id])->id;
        $optionIds[] = factory(App\Option::class)->create(['poll_id' => $poll->id])->id;
        $optionIds[] = factory(App\Option::class)->create(['poll_id' => $poll->id])->id;

        return [
            'poll_id' => $poll->id,
            'option_ids' => $optionIds
        ];
    }

    public function testVoteGetResult()
    {
        $data = $this->createPoll();
        $result = \App\Vote::getResult($data['poll_id']);
        $this->assertCount(count($data['option_ids']), $result);
    }

    public function testVoteGetResultByUser()
    {
        $data = $this->createPoll();
        $result = \App\Vote::getResultByUser($data['poll_id']);
        $this->assertCount(count($data['option_ids']), $result['result']);
        $this->assertCount(0, $result['userMatchedData']);


        $user = factory(App\User::class)->create();
        $optionId = $data['option_ids'][0];
        \App\Vote::doVote($user, $data['poll_id'], $optionId);

        $result = \App\Vote::getResultByUser($data['poll_id'], $user);
        $this->assertCount(count($data['option_ids']), $result['result']);
        $this->assertTrue($result['userMatchedData']['choiceSimilarVolume'] === '100%');
    }

    public function testDoVote()
    {
        $data = $this->createPoll();
        $user = factory(App\User::class)->create();
        $optionId = $data['option_ids'][0];
        $result = \App\Vote::doVote($user, $data['poll_id'], $optionId);
        $this->assertTrue($result === true);
    }
}
