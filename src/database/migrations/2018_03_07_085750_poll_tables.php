<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PollTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('polls', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 100);
            $table->enum('status', ['projected', 'opened', 'closed', 'suspended']);
        });

        DB::connection()->getPdo()->exec("
            INSERT INTO polls (title, status)
            VALUES 
              ('Favorite food', 'projected'),
              ('Music genre preferences', 'opened');
        ");


        Schema::create('options', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('poll_id');
            $table->string('title', 100);

            $table->index('poll_id');
            $table->unique(['poll_id', 'title']);
            $table->foreign('poll_id')->references('id')->on('polls');
        });

        DB::connection()->getPdo()->exec("
            INSERT INTO options (poll_id, title)
            VALUES
              ('2', 'Guitar'),
              ('2', 'Electric'),
              ('2', 'Bass'),
              ('2', 'Banjo');
        ");


        Schema::create('votes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('option_id');
            $table->integer('user_id');
            $table->dateTime('created_at');

            $table->index(['option_id', 'user_id']);
            $table->index('option_id');
            $table->unique(['user_id', 'option_id']);
            $table->foreign('option_id')->references('id')->on('options');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('votes');
        Schema::dropIfExists('options');
        Schema::dropIfExists('polls');
    }
}
