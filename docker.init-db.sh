#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" <<-EOSQL
	CREATE USER musicpoll WITH PASSWORD 'a9d736(s';
    CREATE DATABASE "musicpoll" WITH OWNER "postgres" ENCODING 'UTF8' LC_COLLATE = 'en_US.UTF-8' LC_CTYPE = 'en_US.utf8' TEMPLATE = template0;
	GRANT ALL PRIVILEGES ON DATABASE musicpoll TO musicpoll;
	\connect musicpoll;
EOSQL