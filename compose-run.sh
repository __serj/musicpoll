#!/usr/bin/env bash


FILE_REPLACE=docker-compose.yml
SKIP_COMPOSER=false
SKIP_MIGRATION=false

for i in "$@"
do
case $i in
    -e=*|--env=*)
    APP_ENV="${i#*=}"
    shift # past argument=value
    ;;
    -fa=*|--fileadditional=*)
    FILE_ADDITIONAL="${i#*=}"
    shift # past argument=value
    ;;
    -fr=*|--filereplace=*)
    FILE_REPLACE="${i#*=}"
    shift # past argument=value
    ;;
    -sc|--skip-composer)
    SKIP_COMPOSER=true
    shift # past argument=value
    ;;
    -sm|--skip-migration)
    SKIP_MIGRATION=true
    shift # past argument=value
    ;;
    *)
          # unknown option
    ;;
esac
done


if [ -z "$APP_ENV" ]; then
    echo "Environment is required: 'compose-run.sh -e=dev' or 'compose-run.sh -e=prod'" ; exit
elif [ "$APP_ENV" != "dev" ] && [ "$APP_ENV" != "prod" ]; then
    echo "Environment must be either 'dev' or 'prod'"; exit
fi

export APP_ENV
export ssh_prv_key=$(cat ~/.ssh/id_rsa)
export ssh_pub_key=$(cat ~/.ssh/id_rsa.pub)
export host_ip=$(ifconfig | grep 'inet 192' | awk '{print $2}')

if [ -z  ${FILE_ADDITIONAL} ]; then
    docker-compose -f ${FILE_REPLACE} up -d --build
else
    docker-compose -f ${FILE_REPLACE} -f ${FILE_ADDITIONAL} up -d --build
fi


if [ "$SKIP_COMPOSER" != true ]; then
    docker exec api_laravel_app  bash -c "cd /app/src/ && \
    composer install --no-interaction"
fi

if [ "$SKIP_MIGRATION" != true ]; then
    docker exec api_laravel_app  bash -c "php artisan migrate --force"
fi
