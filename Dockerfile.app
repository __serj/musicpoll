FROM ubuntu:16.04

RUN echo "force-unsafe-io" > /etc/dpkg/dpkg.cfg.d/02apt-speedup
# we don't need an apt cache in a container
RUN echo "Acquire::http {No-Cache=True;};" > /etc/apt/apt.conf.d/no-cache


# fix missing package from ubuntu
# Update and install system base packages
RUN apt-get update && \
    apt-get install -y language-pack-en-base && \
    locale-gen en_US.UTF-8 && \
    export LANG=en_US.UTF-8 && \
    export LC_ALL=en_US.UTF-8 && \
    apt-get install -y software-properties-common && \
    apt-add-repository ppa:ondrej/php && \
    apt-get update && \
    apt-get install -y --no-install-recommends apt-utils \
        iputils-ping \
        ca-certificates \
        ssh \
        mc \
        xz-utils \
        cron \
        git \
        curl \
        nginx \
        phpunit \
        php7.2-fpm \
        php7.2-pgsql \
        php7.2-curl \
        php7.2-xml \
        php7.2-mbstring \
        php7.2-gd \
        php-pear \
        php7.2-dev \
        php-xdebug \
        zip unzip php7.2-zip && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*



# Copy the current directory contents into the container at /app
COPY . /app

# Set the working directory to /app/src
WORKDIR /app/src


# grab arguments from docker-compose
ARG APP_ENV
ARG ssh_prv_key
ARG ssh_pub_key
ARG host_ip

RUN echo "Building ${APP_ENV} enviromnent...";

# nginx log directory
RUN mkdir -m 775 -p /app/logs

RUN chmod -R 775 /app/src/storage

# Install compose
ENV COMPOSER_HOME /root/.composer
ENV PATH /root/.composer/vendor/bin:$PATH
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer


# Xdebug
# to trigger profiler use a quary parameter XDEBUG_PROFILE
RUN echo "xdebug.remote_enable=1" >> /etc/php/7.2/mods-available/xdebug.ini \
    && echo "xdebug.idekey=PHPSTORM" >> /etc/php/7.2/mods-available/xdebug.ini \
    && echo "xdebug.file_link_format=phpstorm://open?%f:%l" >> /etc/php/7.2/mods-available/xdebug.ini \
    && echo "xdebug.remote_host=${host_ip}" >> /etc/php/7.2/mods-available/xdebug.ini \
    && echo "xdebug.remote_connect_back=0" >> /etc/php/7.2/mods-available/xdebug.ini \
    && echo "xdebug.remote_autostart=0" >> /etc/php/7.2/mods-available/xdebug.ini \
    && echo "xdebug.profiler_enable_trigger=1" >> /etc/php/7.2/mods-available/xdebug.ini \
    && echo "xdebug.profiler_output_dir=/app/logs/" >> /etc/php/7.2/mods-available/xdebug.ini

# Configure nginx
ADD docker.nginx.${APP_ENV} /etc/nginx/sites-available/default
RUN echo "daemon off;" >> /etc/nginx/nginx.conf && \
    sed -i.bak 's/upload_max_filesize *= *[a-zA-Z0-9]*/upload_max_filesize = 10M/' /etc/php/7.2/fpm/php.ini && \
    sed -i.bak 's/post_max_size *= *[a-zA-Z0-9]*/post_max_size = 10M/' /etc/php/7.2/fpm/php.ini && \
    sed -i.bak 's/variables_order = "GPCS"/variables_order = "EGPCS"/' /etc/php/7.2/fpm/php.ini && \
    sed -i.bak '/;catch_workers_output = yes/ccatch_workers_output = yes' /etc/php/7.2/fpm/pool.d/www.conf && \
    sed -i.bak 's/log_errors_max_len = 1024/log_errors_max_len = 65536/' /etc/php/7.2/fpm/php.ini


# Create known_hosts
RUN mkdir /root/.ssh && touch /root/.ssh/known_hosts
# Add bitbuckets key
RUN ssh-keyscan bitbucket.org >> /root/.ssh/known_hosts


RUN echo "$ssh_prv_key" > /root/.ssh/id_rsa && \
    echo "$ssh_pub_key" > /root/.ssh/id_rsa.pub && \
    chmod 600 /root/.ssh/id_rsa && \
    chmod 600 /root/.ssh/id_rsa.pub



# Make port 4001 available to the world outside this container
EXPOSE 4001

CMD ["/app/docker.app.sh"]
