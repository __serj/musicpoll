## Deploying the project via Docker
>* change directory to <project root>
* execute bash script:  
  **./compose-run.sh -e=dev**

## Executing tests
>* connect to Docker container:  
  **docker exec -it api\_laravel\_app bash**
* change directory to <project root>/src
* run unit tests:  
  **vendor/bin/phpunit tests**